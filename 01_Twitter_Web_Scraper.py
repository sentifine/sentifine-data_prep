# Command to run this script: python3 Twitter_Web_Scraper.py [start_month] [start_day] [end_month] [end_day] [year]
# Example Set1: python3 test_tweetSoup.py 1 1 6 30 20xx
# Example Set2: python3 test_tweetSoup.py 7 1 12 31 20xx

import sys
import time
import numpy as np
import pandas as pd
from datetime import timedelta, date
from selenium import webdriver
from bs4 import BeautifulSoup

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def main(argv,time):
    # Date
    start_month = int(argv[0])
    start_day = int(argv[1])
    end_month = int(argv[2])
    end_day = int(argv[3])
    start_year = int(argv[4])
    end_year = start_year

    # twitter required params
    base_url = u'https://twitter.com/search?q='
    since = '%20since%3A'
    until = '%20until%3A'
    src = '&src=typd'
    time_sleep = 5
    js_scroll_down = 'window.scrollTo(0, document.body.scrollHeight);'

    # new file header
    #file_from = '-tweet-statuses-from-'
    file_from = '-new-tweet-statuses-from-'
    file_to = '-to-'
    #keywords = '@S3TTR4D3NEWS'
    keywords = '@ThaiValueInvest'

    start_date = date(start_year, start_month, start_day)
    end_date = date(end_year, end_month, end_day)

    browser = webdriver.Firefox()
    # query = u'%40S3TTR4D3NEWS' + since + start_date.strftime('%Y-%m-%d') + until + end_date.strftime('%Y-%m-%d') + src
    query = keywords + since + start_date.strftime('%Y-%m-%d') + until + end_date.strftime('%Y-%m-%d') + src
    url = base_url + query
    browser.get(url)
    browser.maximize_window()  # Scroll down to bottom of page
    lastHeight = browser.execute_script("return document.body.scrollHeight")

    while True:
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        print("Process is sleeping...")
        time.sleep(time_sleep)
        newHeight = browser.execute_script("return document.body.scrollHeight")
        print("Process woke up with NH = " + str(newHeight) + " and LH = " + str(lastHeight))
        if newHeight == lastHeight:
            break
        lastHeight = newHeight

    pho = BeautifulSoup(browser.page_source, 'html.parser')

    # Extract information from url

    dat = pd.DataFrame({'time': [], 'twit': [], 'tag': [], 'at': [], \
                        're': [], 'like': [], 'loc': []})

    for t in pho.find_all('div', {'class': 'content'}):
        time = t.find('a', {'class': 'tweet-timestamp'})['title']
        try:
            twit = t.find('p', {'class': 'TweetTextSize'}).text
        except:
            pass
        tag = []
        for l in t.find_all('a', {'class': 'twitter-hashtag'}):
            tag.append(l.text[1:])
        if tag == []:
            tag = np.NaN
        at = []
        for a in t.find_all('a', {'class': 'twitter-atreply'}):
            try:
                at.append(a['href'][1:])
            except:
                pass
        if at == []:
            at = np.NaN
        thumbs = t.find_all('span', \
                            {'class': 'ProfileTweet-actionCountForPresentation'})
        try:
            re = thumbs[0].text
            if re[-1] == 'K':
                re = int(float(re[:-1]) * 1000)
            else:
                re = int(float(re))
        except:
            pass
        try:
            like = thumbs[2].text
            if like[-1] == 'K':
                like = int(float(like[:-1]) * 1000)
            else:
                like = int(float(like))
        except:
            pass
        if t.find_all('span', {'class': 'Tweet-geo'}) == []:
            loc = np.NaN
        else:
            loc = t.find('span', {'class': 'Tweet-geo'})['title']
        entry = {'time': time, 'twit': twit, 'tag': [tag], 'at': [at], \
                 're': re, 'like': like, 'loc': loc}
        dat = dat.append([pd.DataFrame(entry)], ignore_index=True)

    # Export as a csv
    dat.to_csv(keywords + file_from + start_date.strftime('%Y-%m-%d') + file_to + end_date.strftime('%Y-%m-%d'))
    # , encoding='utf-8'

    print("Finished...")
    browser.close()

if __name__ == "__main__":
    time.time()
    main(sys.argv[1:],time)

# Acknowledgement:
# https://medium.com/@dawran6/twitter-scraper-tutorial-with-python-requests-beautifulsoup-and-selenium-part-2-b38d849b07fe
# https://github.com/mpfarmer/TwSearchEnabled
# https://nycdatascience.com/blog/student-works/twitter-scraping/